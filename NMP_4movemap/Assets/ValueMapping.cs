﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ValueMapping : MonoBehaviour {

	public Slider vSlider;
	float gapV = .1428f;
	public GameObject[] pSyss;
	// Use this for initialization
	void Start () {
			pSyss[0].SetActive(false);
			pSyss[1].SetActive(false);
			pSyss[2].SetActive(false);
			pSyss[3].SetActive(false);
			pSyss[4].SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		float sValue = vSlider.value;
		valueMap(sValue);
	}
	void valueMap(float v){
		
		if(v >= 0 && v <= gapV){
			// Debug.Log("6000BC D");
			pSyss[0].SetActive(true);
			pSyss[1].SetActive(false);
			pSyss[2].SetActive(false);
			pSyss[3].SetActive(false);
			pSyss[4].SetActive(false);
	

		}
		if(v > gapV && v <= gapV*2){
			// Debug.Log("6-5000BC");
			pSyss[0].SetActive(false);
			pSyss[1].SetActive(true);
			pSyss[2].SetActive(false);
			pSyss[3].SetActive(false);
			pSyss[4].SetActive(false);
		}
		if(v > gapV*2 && v <= gapV*3){
			// Debug.Log("5-4000BC");	
			pSyss[0].SetActive(false);
			pSyss[1].SetActive(false);
			pSyss[2].SetActive(true);
			pSyss[3].SetActive(false);
			pSyss[4].SetActive(false);
		}
		if(v > gapV*3 && v <= gapV*4){
			// Debug.Log("4-3000BC");
			pSyss[0].SetActive(false);
			pSyss[1].SetActive(false);
			pSyss[2].SetActive(false);
			pSyss[3].SetActive(true);
			pSyss[4].SetActive(false);
		}
		if(v > gapV*4 && v <= gapV*5){
			// Debug.Log("3-2000BC");
			pSyss[0].SetActive(false);
			pSyss[1].SetActive(false);
			pSyss[2].SetActive(false);
			pSyss[3].SetActive(false);
			pSyss[4].SetActive(true);
		}
		if(v > gapV*5 && v <= gapV*6){
			// Debug.Log("2-1000BC");
			pSyss[0].SetActive(false);
			pSyss[1].SetActive(false);
			pSyss[2].SetActive(false);
			pSyss[3].SetActive(false);
			pSyss[4].SetActive(false);
		}
		if(v > gapV*6 ){
			// Debug.Log("1000BC U");
			pSyss[0].SetActive(false);
			pSyss[1].SetActive(false);
			pSyss[2].SetActive(false);
			pSyss[3].SetActive(false);
			pSyss[4].SetActive(false);
		}
	}
}
