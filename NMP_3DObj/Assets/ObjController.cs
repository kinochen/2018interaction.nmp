﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjController : MonoBehaviour {

	public GameObject oldObj;
	public float speed;
	public GameObject[] relics;
	public int showNum;
	public int maxNum;
	void Awake()
	{
		showNum = 0;
		Cursor.visible = false;
	}
	void Start () {
		changeObj();
	}
	
	// Update is called once per frame
	void Update () {
			//	Rotate by Audience
			oldObj.transform.Rotate( 
				new Vector3( Input.GetAxis("Mouse Y"),	Input.GetAxis("Mouse X"),0) * Time.deltaTime * speed);
			//  AutoRotate
			oldObj.transform.Rotate( 
				new Vector3(1,1,0) * Time.deltaTime *speed /10);

			//change Obj by L or R mousepress
			// change to prev
			if (Input.GetMouseButtonDown(0)){
 				//  Debug.Log("Change to Previous Obj");
					showNum -=1;
					if(showNum <0){
						showNum = maxNum-1;
					}
					changeObj();
			}
          
			// change to next
        	if (Input.GetMouseButtonDown(1)){
					// Debug.Log("Change to Next Obj");
					showNum +=1;
					if(showNum >= maxNum){
						showNum = 0;
					}
					changeObj();
			}
            



	}
	void changeObj(){
		oldObj.SetActive(false);
		oldObj = relics[showNum];
		oldObj.SetActive(true);
	}
}
